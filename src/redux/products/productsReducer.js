import { v4 as uuidv4 } from "uuid";

import {
    FETCH_DATA_PENDING,
    FETCH_DATA_FULFILLED,
    FETCH_DATA_REJECTED,
    ADD_PRODUCT,
    DELETE_PRODUCT,
    EDIT_PRODUCT,
  } from "./productsTypes";

const initialProductState = {
    isLoading: false,
    products: [],
    error: ''
}

const productReducer = (state= initialProductState, action) =>{
    switch (action.type){
        case FETCH_DATA_PENDING:{
            return {
                ...state,
                isLoading:true
            }
        }
        case FETCH_DATA_FULFILLED:{
            return {
                ...state,
                products: action.payload,
                isLoading: false
            }
        }
        case FETCH_DATA_REJECTED:{
            return{
                ...state,
                error: action.payload,
                isLoading:false
            }
        }
        case ADD_PRODUCT:{
            console.log('hello');
            let defaultValues = {
                id: uuidv4(),
                image:
                  "https://user-images.githubusercontent.com/43302778/106805462-7a908400-6645-11eb-958f-cd72b74a17b3.jpg",
                rating: {
                  rate: 0,
                  count: 0,
                },
              }
            return{
                ...state,
                products: [...state.products, {...action.payload,...defaultValues}]
            }
        }
        case DELETE_PRODUCT:{
            const newProducts = state.products.filter((item) => {
                if(item.id !== action.payload.id){
                    return item;
                }
            })
            return {
                ...state,
                products: newProducts,
            }
        }
        case EDIT_PRODUCT:{
            const otherProducts = state.products.filter((item) =>{
                if(item.id != action.payload.id){
                    return item
                }
            })
            return{
                ...state,
                products: [...otherProducts,action.payload],
            }
        }

        default : return state
    }
}

export default productReducer