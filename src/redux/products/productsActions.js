import axios from "axios";
import {
  FETCH_DATA_PENDING,
  FETCH_DATA_FULFILLED,
  FETCH_DATA_REJECTED,
} from "./productsTypes";

export const fetchDataPending = () => {
  return {
    type: FETCH_DATA_PENDING,
  };
};

export const fetchDataFulfilled = (products) => {
  return {
    type: FETCH_DATA_FULFILLED,
    payload: products,
  };
};

export const fetchDataRejected = () => {
  return {
    type: FETCH_DATA_REJECTED,
  };
};

export const fetchProducts = () => {
  return (dispatch) => {
    dispatch(fetchDataPending());
    axios
      .get("https://fakestoreapi.com/products")
      .then((response) => {
        const products = response.data;
        dispatch(fetchDataFulfilled(products));
      })
      .catch((error) => {
        const errorMsg = error.message;
        dispatch(fetchDataRejected(errorMsg));
      });
  };
};

