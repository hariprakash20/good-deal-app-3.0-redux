import { ADD_TO_CART, REMOVE_FROM_CART } from "./cartTypes";

const initalCartState = {
  cart: [],
};

const cartReducer = (state = initalCartState, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let found = false;
      const newCart = state.cart.map((item) => {
        if (item.id === action.payload.id) {
          found = true;
          return { ...item, quantity: item.quantity + 1 };
        } else {
          return item;
        }
      });
      if (newCart.length && found) {
        return {
          ...state,
          cart: newCart,
        };
      } else {
        return {
          ...state,
          cart: [...state.cart, { ...action.payload, quantity: 1 }],
        };
      }
    }

    case REMOVE_FROM_CART: {
      const newCart = state.cart.map((item) => {
        if (item.id === action.payload.id) {
          return { ...item, quantity: item.quantity - 1 };
        } else {
          return item;
        }
      });
      let filteredCart = newCart.filter((item) => {
        if (item.quantity !== 0) {
          return item;
        }
      });
      return {
        ...state,
        cart: filteredCart,
      };
    }
    default:
      return state;
  }
};

export default cartReducer;
