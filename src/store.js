import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./redux/cart/cartReducer";
import productReducer from "./redux/products/productsReducer";

const store = configureStore({
  reducer: {
    products: productReducer,
    cart: cartReducer,
  },
});

export default store;
