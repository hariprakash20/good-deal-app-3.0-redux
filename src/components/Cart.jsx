import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import CartItem from "./CartItem";

export class Cart extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
    
    }
  }
  render() {
    let cart = this.props.cartData.cart;
    let total = 0
    return (
      <>
        <Link to="/">
          <button className="btn-back">back</button>
        </Link>
  
        {cart.length ? (
          <div className="all-cart-items">
            {cart.map((item) => {
              total = total + item.price * item.quantity;
              return <CartItem item={item} key={item.id} />;
            })}
            <div className="total">
              <h1>Grand Total:{total.toFixed(2)}</h1>
            </div>
          </div>
        ) : (
          <h1>no items in cart</h1>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cartData: state.cart,
  };
};

export default connect(mapStateToProps)(Cart);
