import React, { Component } from 'react'
import { connect } from "react-redux";
import { ADD_TO_CART, REMOVE_FROM_CART } from "../redux/cart/cartTypes";



export class CartItem extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       
    }
  }
  render() {
    let item = this.props.item 
    return (
      <div className="cart-item">
        <div className="cart-image-div">
          <img className="image" src={item.image} alt="" />
        </div>
        <div className="product-details">
          <h2>{item.title}</h2>
        </div>
        <div className="billing">
          <h2>${item.price}</h2>
          <h2>
            Quantity: 
            <button className="quantity-change" onClick={() => this.props.addToCart(item)}> + </button>
            {item.quantity}
            <button className="quantity-change" onClick={() => this.props.removeFromCart(item)}> - </button>
          </h2>
          <h2>Sub-Total: {(item.price * item.quantity).toFixed(2)}</h2>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (product) =>
      dispatch({
        type: ADD_TO_CART,
        payload: product,
      }),
    removeFromCart: (product) =>{
      dispatch({
        type: REMOVE_FROM_CART,
        payload: product,
      })
    }
  };
};

export default connect(null,mapDispatchToProps)(CartItem);


  // function handleQuantity(event, id){
  //       setCart((prevCart) => {
  //           let alteredCart = prevCart.map(cartItem =>{
  //               if(cartItem.id === id){
  //                   if(event.target.outerText === '+'){
  //                       return {...cartItem, quantity: cartItem.quantity+1};  
  //                   }
  //                   else{
  //                       if(cartItem.quantity>1)
  //                       return {...cartItem, quantity: cartItem.quantity-1}; 
  //                   }
  //                 }
  //                 else{
  //                   return cartItem;
  //                 }
  //           })
  //           return alteredCart.filter(cartItem =>{
  //               if(cartItem !== undefined)
  //               return cartItem
  //           })

  //       })
  // }