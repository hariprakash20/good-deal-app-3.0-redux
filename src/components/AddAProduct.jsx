import React, { Component } from 'react'
import { Link, Navigate } from "react-router-dom";
import { connect } from 'react-redux';
import { ADD_PRODUCT } from '../redux/products/productsTypes';

export class AddAProduct extends Component {
  constructor(props) {
    super(props)
  
    this.state ={
      product :{
        title: '',
        price: '',
        description: '',
        category:''
      },
      submitted : false,
    } 
  }

  render() {
    if(this.state.submitted){
      return <Navigate replace to="/" />
    }
    else{
      return (
        <>
          <Link to="/"><button className="back-btn">back</button></Link>
          <div>AddAProduct</div>
          <div className="products-details-list">
            <form onSubmit={(event)=> {event.preventDefault(); this.props.addProduct(this.state.product); this.setState({...this.state,submitted: true}) } }>
              <div className="input">
                <label htmlFor="">title :</label>
                <input type="text" name="title" className="input-bar" onChange={(event)=>{ this.setState({ ...this.state , product: {...this.state.product, title:event.target.value} }) }}required />
              </div>
              <div className="input">
                <label htmlFor="">price :</label>
                <input
                  type="number"
                  name="price"
                  step="0.01"
                  className="input-bar"
                  onChange={(event)=>{ this.setState({ ...this.state , product: {...this.state.product, price:event.target.value} }) }}
                  required
                />
              </div>
              <div className="input">
                <label htmlFor="">description :</label>
                <input
                  type="text"
                  name="description"
                  className="input-bar"
                  onChange={(event)=>{ this.setState({ ...this.state , product: {...this.state.product, description:event.target.value} }) }}
                  required
                />
              </div>
              <div className="input">
                <label htmlFor="">category :</label>
                <input
                  type="text"
                  name="category"
                  className="input-bar"
                  onChange={(event)=>{ this.setState({ ...this.state , product: {...this.state.product, category:event.target.value} }) }}
                  required
                />
              </div>
              <button className="submit-btn" type="submit">submit</button>
            </form>
          </div>
        </>
      );
    }
    
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.products,
  };
};

const mapDispatchToProps = (dispatch) =>{
  return{
    addProduct: (product) => dispatch({
      type: ADD_PRODUCT,
      payload: product
    })
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(AddAProduct);