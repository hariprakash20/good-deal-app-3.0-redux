import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, Navigate } from "react-router-dom";
import { EDIT_PRODUCT } from "../redux/products/productsTypes";

export class EditTheProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {};
    
  }
  initializeState = (id = window.location.pathname.split("/")[2]) => {
    const selectedProduct = this.props.productsData.products.filter((item) => {
      if (item.id == id) {
        return item;
      }
    })[0];
    this.setState({product: selectedProduct, submitted: false});
  };
  componentDidMount(){
    this.initializeState();
  }
  render() {
    if(this.state.submitted){
      return <Navigate replace to="/" />
    }
    else if(this.state.product){
      return (
        <>
          <Link to="/">
            <button className="back-btn">back</button>
          </Link>
          <div>Edit A Product</div>
          <div className="products-details-list">
            <form
              onSubmit={(event) => {
                event.preventDefault();
                this.props.editProduct(this.state.product);
                this.setState({...this.state, submitted: true})
              }}
            >
              <div className="input">
                <label htmlFor="">title :</label>
                <input
                  type="text"
                  name="title"
                  className="input-bar"
                  defaultValue={this.state.product.title}
                  onChange={(event) => {
                    this.setState({ ...this.state,product: {...this.state.product,  title: event.target.value  }});
                  }}
                  required
                />
              </div>
              <div className="input">
                <label htmlFor="">price :</label>
                <input
                  type="number"
                  name="price"
                  step="0.01"
                  className="input-bar"
                  defaultValue={this.state.product.price}
                  onChange={(event) => {
                    this.setState({ ...this.state,product: {...this.state.product,  price: event.target.value  }});
                  }}
                  required
                />
              </div>
              <div className="input">
                <label htmlFor="">description :</label>
                <input
                  type="text"
                  name="description"
                  className="input-bar"
                  defaultValue={this.state.product.description}
                  onChange={event => {this.setState({ ...this.state,product: {...this.state.product,  description: event.target.value  }});}}
                  required
                />
              </div>
              <div className="input">
                <label htmlFor="">category :</label>
                <input
                  type="text"
                  name="category"
                  className="input-bar"
                  defaultValue={this.state.product.category}
                  onChange={event => {this.setState({ ...this.state,product: {...this.state.product,  category: event.target.value  }});}}
                  required
                />
              </div>
              <button className="submit-btn" type="submit">
                submit
              </button>
            </form>
          </div>
        </>
      )
    }else{
      return <>no item available</>
    }
    
  }
}

const mapStateToProps = (state) => {
  return {
    productsData: state.products,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    editProduct: (product) => dispatch({
      type: EDIT_PRODUCT,
      payload: product
    })
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(EditTheProduct);
