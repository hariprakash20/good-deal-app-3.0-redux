import React, { Component } from 'react'
import "./style.css";
import { Link } from "react-router-dom";
import { connect } from 'react-redux';

export class Navbar extends Component {

  getTotal(){
    return this.props.cartData.cart.reduce((accumlator, item)=> {
      return accumlator+item.quantity
    },0)
  }

  render() {
    const itemCount = this.getTotal();

    return (
      <div className="navbar">
        <div className="logo">
          <i className="fa-regular fa-handshake"></i>Good deal
        </div>
        <div className="search">
          <input type="text" />
          <button>
            <i className="fa-solid fa-magnifying-glass"></i>
          </button>
        </div>
        <div className="nav-options">
        <div className="categories">
          <select name="categories" id="">
            <option value="">Categories</option>
          </select>
        </div>
        <Link to="/cart">
          <div className="cart">
            <i className="fa-solid fa-cart-shopping"></i>
            <button className="badge">{itemCount}</button>
          </div>
        </Link>
          
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) =>{
  return{
    cartData : state.cart
  }
}

export default connect(mapStateToProps)(Navbar)
