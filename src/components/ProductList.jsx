import React, { Component } from 'react'
import { connect } from 'react-redux';
import './style.css'
import ProductTile from './ProductTile'
import { API_STATES } from '../API_STATES'

class ProductList extends Component {  
  render() {
    const products = this.props.productsData.products;
    return (
      <>
          <div className="products-list">
          {products.status === API_STATES.LOADING ? (
            <div className="loader"></div>
          ) : products.status === API_STATES.ERROR ? (
            <h1>Error occured in Fetching the data</h1>
          ) 
          : 
          products ? (
            products.map((product) => (
              <ProductTile product={product} key={product.id}/>
            ))
          ) :
           (
            <h1>No products to display</h1>
          )
          }
          </div>
        </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    productsData: state.products,
  };
};

export default connect(mapStateToProps,null)(ProductList)




