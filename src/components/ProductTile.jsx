import React, { Component } from "react";
import "./style.css";
import { ADD_TO_CART } from "../redux/cart/cartTypes";
import { connect } from "react-redux";
import { DELETE_PRODUCT } from "../redux/products/productsTypes";
import { Link } from "react-router-dom";

export class ProductTile extends Component {
  render() {
    let product = this.props.product;
    const bg = "card bg-light";
    return (
      <>
        <div className={bg}>
          <div className="edit-and-delete">
            <Link to={`/editTheProduct/${product.id}`}>
              <i className="fa-solid fa-pen-to-square edit-btn"></i>
            </Link>
            <i
              className="fa-solid fa-trash delete-btn"
              onClick={() => this.props.deleteProduct(product)}
            ></i>
          </div>
          <div className="product-info">
            <img className="product-image" src={product.image} alt="img" />
            <div className="product-title">
              <b>{product.title}</b>
            </div>
            <div className="price">${product.price}</div>
            <div className="rating">
              <i className="fa-solid fa-star star"></i>
              <span className="rate">{product.rating.rate}</span>
              <span className="count">({product.rating.count})</span>
            </div>
          </div>

          <div className="buttons">
            <button
              className="add-to-cart"
              onClick={() => this.props.addToCart(product)}
            >
              Add to cart
            </button>
            <button className="buy-now">Buy Now</button>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (product) =>
      dispatch({
        type: ADD_TO_CART,
        payload: product,
      }),
    deleteProduct: (product) =>
      dispatch({
        type: DELETE_PRODUCT,
        payload: product,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductTile);



