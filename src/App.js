import React, { Component } from "react";
import "./App.css";
import { connect } from "react-redux";
import { fetchProducts } from "./redux/products/productsActions";
import Navbar from "./components/Navbar";
import ProductList from "./components/ProductList";
import { Routes, Route, Link } from "react-router-dom";
import Cart from "./components/Cart";
import  AddAProduct  from "./components/AddAProduct";
import EditTheProduct from "./components/EditTheProduct";



export class App extends Component {
  componentDidMount() {
    this.props.fetchProducts();
  }

  render() {
    return (
      <div className="App">
      <Navbar />
        <Link to="/addAProduct">
          <button className="add-a-product">Add a product</button>
        </Link>
        <Routes>
          <Route path="/" element={<ProductList />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/addAProduct" element={<AddAProduct  />} />
          <Route path="/editTheProduct/:id" element={<EditTheProduct />}/>
        </Routes>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    productsData: state.products.products,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProducts: () => dispatch(fetchProducts()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
